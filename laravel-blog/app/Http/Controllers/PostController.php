<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access the authenticated user via Auth Facade
use illuminate\Support\Facades\Auth;

use App\Models\Post;
use PHPUnit\Framework\TestStatus\Success; 
use App\Models\PostLike;
use App\Models\PostComment;


class PostController extends Controller
{

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()){
            // instantiate a new post object from the Post Model
            $post = new Post();
            // define the properties of the $post object using the received form data.
            $post->title = $request->title;
            $post->content = $request->content;
            //get the id of the authenticated user and set it as the foreign key for the user_id of the new post.
            $post->user_id = Auth::user()->id;
            $post->save();
            // save this $post object into the database.
            return redirect('/posts');
        }else {
            return redirect('/login');
        }   
    }

    // action that will return a view showing all the blog posts.
    public function index()
    {
        $posts = Post::where('isActive', true)->get();
        return view('posts.index')->with('posts', $posts);
    }


    public function myPosts()
    {
        if(Auth::user())
        {
            $posts = Auth::user()->posts;
            $posts = Post::where('isActive', true)->get();
            return view('posts.index')->with('posts', $posts);

        }else {
            return redirect('/login');
        }

}   


        public function show($id)
        {
            $post = Post::find($id);
            return view('posts.show')->with('post', $post);
        }
        

        public function edit($id)
        {
            $post = Post::find($id);
            
            if (!Auth::user()) {
                return redirect('/login');
            }
           
            if (Auth::user()->id !== $post->user_id) {
                return redirect('/posts');
            } 
            return view('posts.edit', compact('post'));
        }


        public function update(Request $request, $id)
        {
            
            if(Auth::user()){
            $post = Post::find($id);

            $validatedData = $request->validate([
                'title' => 'required|max:255',
                'content' => 'required',
            ]);

            $post->title = $validatedData['title'];
            $post->content = $validatedData['content'];
            $post->save();

            return redirect('/');
        }
        else {
            return redirect('/login');
        }

        }

        public function archive($id)
        {
            $post = Post::find($id);
        
            if (Auth::user()->id == $post->user_id) {
                $post->isActive = false;
                $post->save();
            }
        
            return redirect('/posts');
        }

        public function like($id)
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;


        //if authenticated user is not the post author
        if($post->user_id != $user_id)
        {
            //checks if a post like has been made by the logged in user
            if($post->likes->contains("user_id", $user_id)) {
                //deletes the like mad by the user to unlike the post.
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
            } else {
                //to instantiate a new PostLike object from the postLike model.
                $postLike = new PostLike;
                //define the properties of the $postLike object
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                //save the $postLike object into the database
                $postLike->save();
            }
            //redirect user back to the original post page
            return redirect("/posts/$id");
        }
    }


    public function comment(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $post = Post::find($id);
    
    
        $comment = new PostComment;
    
        $comment->post_id = $post->id;
        $comment->user_id = $user_id;
        $comment->content = $request->input('content');
        $comment->save();
    
        return redirect("/posts/$id");
    }
}
