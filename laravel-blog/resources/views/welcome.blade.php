@extends('layouts.app')

@section('content')

<div class="banner text-center my-4">
    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" style="max-width: 200px;">
</div>

    <div class="featured-posts text-center container">
        <h2 class="text-center mb-4">Featured Posts</h2>
        @if(count($posts) > 0)
            @foreach($posts->random(min(3, count($posts))) as $post)
            @if($post->isActive == true)
                <div class="card mb-3">
                    <div class="card-body">
                        <h4 class="card-title"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text">Author: {{$post->user->name}}</h6>
                    </div>
                </div>
                @endif
            @endforeach
        @else
            <div>
                <h2>There are no posts to show</h2>  
                <a href="/posts/create" class="btn btn-info">Create post</a>  
            </div> 
        @endif
    </div>
@endsection


