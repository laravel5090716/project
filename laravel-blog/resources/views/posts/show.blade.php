{{-- @extends('layouts.app')

@section('content')





    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p>Author: {{$post->user->name}}</p>
            <p>Created at {{$post->created_at}}</p>
            <p>{{$post->content}}</p>

            @if(Auth::id() != $post->user_id)

            <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

                @method('PUT')
                @csrf

                @if($post->likes->contains("user_id", Auth::id()))
                    <button type="submit" class="btn btn-danger">Unlike</button>
                @else 
                    <button type="submit" class="btn btn-success">Like</button>
                @endif
                
                </form>


            @endif




            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>


@endsection --}}


{{-- @extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p>Author: {{$post->user->name}}</p>
        <p>Created at {{$post->created_at}}</p>
        <p>{{$post->content}}</p>

        @if(Auth::id() != $post->user_id)

        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

            @method('PUT')
            @csrf

            @if($post->likes->contains("user_id", Auth::id()))
                <button type="submit" class="btn btn-danger">Unlike</button>
            @else 
                <button type="submit" class="btn btn-success">Like</button>
            @endif
            
        </form>

        @endif

        <div class="mt-3">
            <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <h2 class="card-title">Comments</h2>

        @foreach($post->comments as $comment)
        <div class="mb-3">
            <p>{{$comment->user->name}} said:</p>
            <p>{{$comment->content}}</p>
        </div>
        @endforeach

        <!-- Button trigger modal -->
        @if(Auth::id() != $post->user_id)
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentModal">
            Add Comment
        </button>

        <!-- Modal -->
        <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/posts/{{$post->id}}/comment">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="content">Comment:</label>
                                <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endif
@endsection --}}


{{-- @extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p>Author: {{$post->user->name}}</p>
        <p>Created at {{$post->created_at}}</p>
        <p>{{$post->content}}</p>

        @if(Auth::id() != $post->user_id)

        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

            @method('PUT')
            @csrf

            @if($post->likes->contains("user_id", Auth::id()))
                <button type="submit" class="btn btn-danger">Unlike</button>
            @else 
                <button type="submit" class="btn btn-success">Like</button>
            @endif
            
        </form>

        @endif

        <div class="mt-3">
            <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <h2 class="card-title">Comments</h2>

        @foreach($post->comments as $comment)
        <div class="mb-3">
            <p>{{$comment->user->name}} said:</p>
            <p>{{$comment->content}}</p>
        </div>
        @endforeach

        <!-- Add Comment Form -->
        @if(Auth::id() != $post->user_id)
        <form method="POST" action="/posts/{{$post->id}}/comment">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="content">Add Comment:</label>
                <textarea class="form-control" id="content" name="content" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        @endif

    </div>
</div>

@endsection --}}


@extends('layouts.app')

@section('content')

<div class="card">
    <div class="card-body">
        <h2 class="card-title">{{$post->title}}</h2>
        <p>Author: {{$post->user->name}}</p>
        <p>Created at {{$post->created_at}}</p>
        <p>{{$post->content}}</p>

        @if(Auth::id() != $post->user_id)

        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">

            @method('PUT')
            @csrf

            @if($post->likes->contains("user_id", Auth::id()))
                <button type="submit" class="btn btn-danger">Unlike</button>
            @else 
                <button type="submit" class="btn btn-success">Like</button>
            @endif
            
        </form>

        @endif

        <div class="mt-3">
            <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-body">
        <h2 class="card-title">Comments</h2>

        @foreach($post->comments as $comment)
        <div class="mb-3">
            <p>{{$comment->user->name}} said:</p>
            <p>{{$comment->content}}</p>
        </div>
        @endforeach

        <!-- Button trigger modal -->
        @if(Auth::id() != $post->user_id)
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
            Add Comment
        </button>

        <!-- Modal -->
        <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="commentModalLabel">Add Comment</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="/posts/{{$post->id}}/comment">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="content">Comment:</label>
                                <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endif
@endsection