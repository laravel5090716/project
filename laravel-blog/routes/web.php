<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $posts = App\Models\Post::all();
    return view('welcome', compact('posts'));
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//define a route wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class,'index']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class,'myPosts']);


Route::get('/posts/{id}', [PostController::class,'show']);

Route::get('/posts/{id}/edit', [PostController::class, 'edit'])->name('post.edit');


//define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update'])->name('posts.update');


// define a route that will delete a post of the matching URL parameter ID.
Route::delete('/posts/{id}', [PostController::class, 'archive']);


Route::put('/posts/{id}/like', [PostController::class, 'like']);


Route::put('/posts/{id}/comment', [PostController::class, 'comment']);